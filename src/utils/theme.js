const colors = {
  primary: "#1E9DA5",
  secondary: "#769553",
  accent: "#715295",
  primaryDark: "#3C4459",
  white: "#FFFFFF",
  black: "#000000",
};

const fonts = {
  light: "OperatorMono-Light",
  lightItalic: "OperatorMono-LightItalic",
  regular: "OperatorMono-Book",
  medium: "OperatorMono-Medium",
  bold: "OperatorMono-Bold",
};

export { colors, fonts };
