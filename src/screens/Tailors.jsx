import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ImageBackground,
} from "react-native";
import React from "react";

const Tailors = () => {
  const tailors = [
    {
      tailorName: "John",
      tailorOrderCount: 5,
      tailorPicture:
        "https://cdn.pixabay.com/photo/2019/03/22/11/03/dressmaker-4073022_1280.jpg",
    },
    {
      tailorName: "Jane",
      tailorOrderCount: 3,
      tailorPicture:
        "https://cdn.pixabay.com/photo/2015/08/25/22/36/sewing-907803_1280.jpg",
    },
    {
      tailorName: "Bob",
      tailorOrderCount: 7,
      tailorPicture:
        "https://cdn.pixabay.com/photo/2013/07/25/11/52/threads-166858_1280.jpg",
    },
    {
      tailorName: "Sue",
      tailorOrderCount: 2,
      tailorPicture:
        "https://cdn.pixabay.com/photo/2017/09/23/12/34/tailor-2778734_1280.jpg",
    },
    {
      tailorName: "Sue",
      tailorOrderCount: 2,
      tailorPicture:
        "https://cdn.pixabay.com/photo/2017/09/23/12/34/tailor-2778734_1280.jpg",
    },
  ];

  const __renderItem = ({ item }) => (
    <ImageBackground
      style={styles.itemCon}
      source={{ uri: item.tailorPicture }}
    >
      <View style={styles.tailerOrderBadgeCon}>
        <Text>{item.tailorOrderCount}</Text>
      </View>

      <View style={styles.itemTitleCon}>
        <Text>{item.tailorName}</Text>
      </View>
    </ImageBackground>
  );

  return (
    <View style={styles.con}>
      <FlatList data={tailors} renderItem={__renderItem} numColumns={3} />
    </View>
  );
};

export default Tailors;

const styles = StyleSheet.create({
  con: {
    flex: 1,
    marginTop: 20,
    backgroundColor: "red",
  },

  itemCon: {
    width: 150,
    height: 160,
    marginHorizontal: 10,
    justifyContent: "space-between",
  },

  itemTitleCon: {
    backgroundColor: "rgba(255,255,255,0.8)",
    padding: 10,
    alignItems: "center",
  },

  tailerOrderBadgeCon: {
    width: 25,
    height: 25,
    borderRadius: 12.5,
    backgroundColor: "rgba(0,255,0,1)",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-end",
  },
});
